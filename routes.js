const { Router } = require('express');
const { get } = require('./controllers/get');
const { inicio } = require('./controllers/inicio');

const routes = Router();

routes.get('/', get);

routes.get('/inicio', inicio)

routes.get('/fin', (req, res) => {
    res.status(301).json({message:'fin'});
})

routes.post('/inicio', (req, res) => {
    res.status(301).json({message:'inicio'});
})


module.exports = routes;